# Introdução
Foi desenvolvido um alimentador automatico de animais, com display lcd para saida de dados e teclado numerico para o usuario programar o horario da alimentação.
## Equipe

O projeto foi desenvolvido pelo aluno de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Arthur Klipp|arthurklipp@alunos.utfpr.edu.br|

# Documentação

A documentação do projeto pode ser acessada pelo link:

> https://hellraiser.gitlab.io/ie21cp20202/

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)

