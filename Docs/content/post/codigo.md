---
title: "Codigo"
date: 2021-05-13T21:05:05-03:00
draft: false
---

~~~C
#include <Keypad.h>
#include <LiquidCrystal.h>
#define LIN 4
#define COL 4
#define MOTOR 12

LiquidCrystal lcd(7, 6, 5, 4, 3, 2);

char teclas[LIN][COL] = {{1,2,3,'A'},{4,5,6,'B'},{7,8,9,'C'},{'*',10,'#','D'}};
byte linhas[LIN] = {11, 10, 9, 8};
byte colunas[COL] = {A2, A3, A4, A5};
Keypad teclado = Keypad(makeKeymap(teclas), linhas, colunas, LIN, COL);

void setup(){
  Serial.begin(9600);
  lcd.begin(16, 2);
  pinMode(MOTOR, OUTPUT);
}

int hora[2], min[2], TEMPO=4000, chave=0;

void loop(){
  if(hora[0]==0 && min[0]==0){
    lcd.setCursor(0, 0);
    lcd.print("Tempo: ");
    lcd.setCursor(0, 1);
  	lcd.print("Indefinido");
  }else{
    imprimirHorario();
    tempo();
    acionarMotor();
  }

  char tecla = teclado.getKey();

  if(tecla){
    if(tecla=='A'){
      horario();
    }
  }
}

void horario(void){
  int i=0;
  hora[0]=0;
  hora[1]=0;
  min[0]=0;
  min[1]=0;

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Horas: ");
  lcd.setCursor(0,1);
  do{
    char tecla = teclado.getKey();
    if(tecla!='*'&&tecla!='#'&&tecla!='A'&&tecla!='B'&&tecla!=0){
      if(tecla=='D'){
        lcd.clear();
  	  	lcd.setCursor(0,0);
      	lcd.print("Horas: ");
      	lcd.setCursor(0,1);
        hora[0]=0;
        hora[1]=0;
        i=0;
      }else{
        if(tecla==10){
          tecla=0;
        }
        if(tecla!='C'){
          hora[i]=tecla;
          lcd.print(hora[i]);
          i++;
        }else{
          if(i==2){
            hora[0]=((hora[0]*36000)+(hora[1]*3600))/3600;
            chave=1;
          }else{
            chave=1;
          }
        }
      }
    }
  }while(chave==0);
  minutos();
}

void minutos(void){
  int i=0;
  chave=0;
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Minutos: ");
  lcd.setCursor(0,1);
  do{
    char tecla = teclado.getKey();
    if(tecla!='*'&&tecla!='#'&&tecla!='A'&&tecla!='B'&&tecla!=0){
      if(tecla=='D'){
        lcd.clear();
  	  	lcd.setCursor(0,0);
      	lcd.print("Minutos: ");
      	lcd.setCursor(0,1);
        min[0]=0;
        min[1]=0;
        i=0;
      }else{
        if(tecla==10){
          tecla=0;
        }
        if(tecla!='C'){
          min[i]=tecla;
          lcd.print(min[i]);
          i++;
        }else{
          if(i==2){
            min[0]=((min[0]*600)+(min[1]*60))/60;
            chave=1;
          }else{
            chave=1;
          }
        }
      }
    }
  }while(chave==0);
  chave=0;
}

void tempo(void){
  float tempo=hora[0]*36000+min[0]*600;
  chave=0;

  do{
    char tecla = teclado.getKey();
    if(tecla=='A'){
      horario();
      imprimirHorario();
      tempo=hora[0]*36000+min[0]*600;
    }
    delay(100);
    tempo=tempo-1;
    if(tempo==0){
      chave=1;
    }else{
      Serial.println("Tempo restante");
      Serial.println(tempo/10);
    }
  }while(chave==0);

  chave=0;
}

void acionarMotor(void){
  lcd.clear();
  lcd.print("Alimentando");
  digitalWrite(MOTOR, HIGH);
  delay(TEMPO);
  digitalWrite(MOTOR, LOW);
  lcd.clear();
}

void imprimirHorario(void){
  lcd.clear();
  lcd.print("Tempo:");
  lcd.setCursor(0,1);

  if(hora[0]<=9){
    lcd.print(0);
    lcd.print(hora[0]);
  }else{
    lcd.print(hora[0]);
  }

  lcd.print(":");

  if(min[0]<=9){
    lcd.print(0);
    lcd.print(min[0]);
  }else{
    lcd.print(min[0]);
  }
}
~~~