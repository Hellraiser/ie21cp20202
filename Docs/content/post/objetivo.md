---
title: "Objetivo"
date: 2021-05-13T21:05:20-03:00
draft: false
---

O objetivo do projeto foi desenvolver um alimentador de animais automatico semelhante ao do [manual do mundo](https://www.youtube.com/watch?v=TdMz7EMKBdY&t=672s). Porem com alguns pontos melhorados, em que o usuario nao depende de um computador e nem de conhecimento em programação para escolher os horarios em que deseja alimentar seu animal, apenas informando o intervalo de tempo atraves de um teclado númerico já é possivel fazer essa tarefa. Além de ser possivel controlar a quantidade de ração que será despejada no pote, utilizando de um potenciometro ligado ao motor e tambem um botao, onde pode ser acionado o motor. O alimentador possui as seguintes funções: A(alterar horario), C(confirmar horario), D(deletar horario). O motor sempre sera acionado apos terminar o intervalo de tempo(exemplo: alimentar de 12 em 12 horas, 24 em 24 horas e assim por diante).
