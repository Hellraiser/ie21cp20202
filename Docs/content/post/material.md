---
title: "Material"
date: 2021-05-13T21:05:15-03:00
draft: false
---

* 1 arduino uno R3  
* 1 keypad 4x4  
* LCD 16x2  
* 1 relé SPDT  
* 1 potenciometro de 500 ohms  
* 1 potenciometro de 250k ohms  
* 1 resistor de 220 ohms  
* 1 bateria de 9 volts  
* 1 botao de 4 terminais  
* 1 motor de engrenagem  