---
title: "Introdução: "
date: 2021-05-13T21:05:24-03:00
draft: false
---

O prototipo desenvolvido é um alimentador de animais automatico. O mesmo constitui de um teclado numerico para o usuario inserir o horario da alimentação(em intervalo de tempo), e um display LCD para informar o horario e quando está alimentando o animal. O projeto possui aparencia semelhante ao que foi produzido no [manual do mundo](https://www.youtube.com/watch?v=TdMz7EMKBdY&t=672s), possuindo tubo para estoque de ração e um motor conectado a um parafuso de arquimedes para descarregar a ração.   
Foto do parafuso de arquimedes:
{{< figure src="img1.png" width="400px">}}  
Alimentador automatico conectado ao computador:  
{{< figure src="img2.png" width="400px">}}  